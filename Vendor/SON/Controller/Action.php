<?php

namespace SON\Controller;

class Action{
    
    protected $view;
    protected $action;
    
    public function __construct(){
        
        $this->view = new \stdClass;        
    }
    
    public function render($action, $layout=true){
        
        $this->action = $action;
        
        if($layout == true && file_exists("../APP/Views/layout.phtml")){
            
            include_once "../APP/Views/layout.phtml";
            
        }else{
            
            $this->content();
        }
        
    }
    
    public function content(){
        
        $atual = get_class($this);
        $singleClassName = strtolower(str_replace("APP\\Controllers\\","",$atual));
        include_once "../APP/Views/".$singleClassName.'/'.$this->action.'.phtml';
    }
}