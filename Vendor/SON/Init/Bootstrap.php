<?php

namespace SON\Init;

//alterado para abstract por ser uma classe modelo para o Init
abstract class Bootstrap{
 
    //atributo que recebe a rota procurada
    private $routes;
    
    //função que constroi e executa as actions=metodos da classe Init
    public function __construct(){
        $this->initRoutes();
        $this->run($this->getUrl());
    }
    
    /**aqui colocamos as rotas que podem ser acessadas / encaminhamento para os controllers models ou view
    foi alterado para abstract protected para ser acessado externo porem com restrições
    */ 
    abstract protected function initRoutes();
    
     //metodo set para o atributo route da classe Init
    protected function setRoutes(array $routes){
        $this->routes = $routes;
    }
    
    //compara se a url solicitada pertence ao array de rotas de initrotas
    protected function run($url){
        
        array_walk($this->routes, function($route) use($url){
            
            if($url == $route['route']){
                $class = "APP\\Controllers\\".ucfirst($route['controller']);
                $controller = new $class;
                $controller->{$route['action']}();
            }
        });
    }
       
    //sequestro da rota/url digitada pelo usuário
    protected function getUrl(){
        
        return parse_url($_SERVER['REQUEST_URI'],PHP_URL_PATH);
    } 
}