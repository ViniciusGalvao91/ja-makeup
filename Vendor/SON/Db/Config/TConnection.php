<?php

namespace SON\Db\Config;

use Exception;

final class TConnection{
    
    
    private function __construct(){
        
                
    }
    
    public static function open($name){

      try{
            
            $db = new \PDO("mysql:host=localhost;dbname={$name}","root","");
          
            $db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            return $db;
            
        }catch (Exception $e){

                echo '<b>Falha: Verifique a conexão com o banco de dados!!!</b> ' . utf8_encode($e->getMessage());                           
            
        }        
    }    
}

?>