<?php
    
function __autoload($classe){
    
    if(file_exists("app.ado/{$classe}.class.php")){
        
        include_once "app.ado/{$classe}.class.php";
        
    }    
    
    if(file_exists("app.config/{$classe}.class.php")){
        
        include_once "app.config/{$classe}.class.php";
        
    }    
}

try{
    
    TTransaction::open('my_jamakeup');
        
    //insert na base
    $sql = new TSqlInsert;
    $sql->setEntity('cliente');
    $sql->setRowData('nome', 'Larissa');
    $sql->setRowData('celular', '985192156');
    $sql->setRowData('telefone', '26933021');
    $sql->setRowData('email', 'larissaab@gmail.com');
    $sql->setRowData('cpf', '15323689652');
    $sql->setRowData('obs', 'Teste de consistencia');
    
    $conn = TTransaction::get();    
    $result = $conn->Query($sql->getInstruction());
    
    //update na base
    $sql = new TSqlUpdate;
    $sql->setEntity('cliente');
    $sql->setRowData('nome_', 'Larissa Barreto Vandjal');
    
    $criteria = new TCriteria();
    $criteria->add(new TFilter('id', '=', 12));
    $sql->setCriteria($criteria);
    
    $conn = TTransaction::get();
    $result = $conn->Query($sql->getInstruction());
    
    
    TTransaction::close();
          
}catch(Exception $e){
        
    print " ERRO: " . $e->getMessage() . "<br>\n";
    TTransaction::rollback();
}

?>