<?php
    
function __autoload($classe){
    
    if(file_exists("app.ado/{$classe}.class.php")){
        
        include_once "app.ado/{$classe}.class.php";
        
    }    
    
    if(file_exists("app.config/{$classe}.class.php")){
        
        include_once "app.config/{$classe}.class.php";
        
    }    
}

try{
    
    TTransaction::open('jamakeup');
    
    TTransaction::setLogger(new TLoggerTXT('tmp/arquivo.txt'));
    
    TTransaction::log("Vinicius Galvão");
        
    //insert na base
    $sql = new TSqlInsert;
    $sql->setEntity('cliente');
    $sql->setRowData('nome', 'Vinicius Galvão');
    $sql->setRowData('celular', '983238014');
    $sql->setRowData('telefone', '26933021');
    $sql->setRowData('email', 'vinicius.galvaodeoliveira@gmail.com');
    $sql->setRowData('cpf', '134125607313');
    $sql->setRowData('obs', 'Escrevendo em arquivo txt, parece muito facil');
    
    $conn = TTransaction::get();    
    $result = $conn->Query($sql->getInstruction());
    
    TTransaction::log($sql->getInstruction());    
    
    TTransaction::close();
          
}catch(Exception $e){
        
    print " ERRO: " . $e->getMessage() . "<br>\n";
    TTransaction::rollback();
}



?>