<?php

namespace APP\Controllers;

use SON\Controller\Action;
use \SON\Di\Container;


class Financeiro extends Action{
        
    //envia para a tela o conteudo retornado da classe init
    public function financeiro(){
                
        //action que desejo renderizar
        //$this->render('financeiro');
        
        $this->Novo_lancamento();
    }  
    
    public function Novo_lancamento(){
                
        //action que desejo renderizar
        $this->render('Novo_lancamento');
    }  
    
     public function Alterar_Lancamento(){
       
        //renderizando
        $this->render('Alterar_Lancamento');
    }
        
    public function extratos(){
                
        //action que desejo renderizar
        $this->render('extratos');
    }  
    
    //envia para a tela o conteudo retornado da classe init
    public function salvar(){
             
        if(isset($_POST['dataLiberacaoPagSeguro'])){
            
            $pagseguro =  $_POST['dataLiberacaoPagSeguro'];
        }else{
            
            $pagseguro = "";
        }
        
        if(isset($_POST['id'])){
            
            $id =  $_POST['id'];
        }else{
            
            $id = "";
        }
                        
        $movimento = Container::getClass("Financeiro");
                                             
        $result = $movimento->gravar($id, utf8_decode($_POST['descricao']), $_POST['dataMovimento'], utf8_decode($_POST['tipoMovimento']), $_POST['valorMovimento'], $_POST['codAtendimento'], utf8_decode($_POST['meioPagamento']), $pagseguro);                                  
        $this->view->retorno = $result;
        
        $this->render('Novo_lancamento');
                 
    }
    
    public function listar(){
                                         
        $lancamento = Container::getClass("Financeiro");
        
        $lancamentos = $lancamento->getLancamentos();
            
        $this->view->lancamentos = $lancamentos;
            
        $this->render('listar');  
    } 
    
    public function pesquisa(){
                               
                    
        $cliente = Container::getClass("Cliente");
        
        $clientes = $cliente->fetAll();        
        
    
        $this->view->clientes = $clientes;
        
        //renderizando
        $this->render('pesquisa');
    }
    
    public function pesquisaConsulta(){
                               
        $cliente = Container::getClass("Cliente");
                
        if($clientes = $cliente->find($_POST['id']) == null){
            
           echo ("<script>alert('Cliente não encontrada!');</script>");
            
          
        }else{
        
        $clientes = $cliente->find($_POST['id']);
            //envia os dados para a view
        $this->view->clientes = $clientes;
        
        //renderizando
        $this->render('consulta');
            
        }
    }

     public function Consulta(){
                               
        $lancamento = Container::getClass("Financeiro");
        
        $result = $lancamento->getLancamento($_GET['LancamentoId']);
            
        //envia os dados para a view
        $this->view->lancamento = $result;
        
        $this->render('Alterar_Lancamento');
                    
    }
    
}