<?php

namespace APP\Controllers;

use SON\Controller\Action;
use \SON\Di\Container;


class Servicos extends Action{
        
    //envia para a tela o conteudo retornado da classe init
    public function Servicos(){
        
        //action que desejo renderizar
        //$this->render('Servicos');
        $this->Novo_Servico();
    }
    
    public function Novo_Servico(){
        
        
      $this->render('Novo_Servico');  
        
    }
    
     public function Listar_Servicos(){
                                         
        $servico = Container::getClass("Servicos");
                    
        $this->view->servicos = $servico->getServicos();
            
        $this->render('listar');  
    }
        
    public function Alterar_Servico(){
        
        
      $this->render('Alterar_Servico');  
        
    }
               
    public function salvar(){
             
        if(isset($_POST['status'])){
            
            $status = $_POST['status'];
             
        }else{
            
            $status = "0";
                    
        }
        
        $servico = Container::getClass("Servicos");

        $result = $servico->salvar($_POST['id'], utf8_decode($_POST['nomeServico']), utf8_decode($_POST['descricaoServico']), $_POST['valorServico'], $status);
                
        $this->view->retorno = $result;
        
        $this->render('Novo_Servico');
                
    }
            
    public function Consulta(){
        
        $id = $_GET["ServicoId"];
        
        $servico = Container::getClass("Servicos");
                
        $dado = $servico->listarServico($id);
        
        $this->view->servico = $dado;
       
        $this->render('Alterar_Servico');
    }
    
    public function Consultajson(){
        
        $id = $_GET["ServicoId"];
        
        $servico = Container::getClass("Servicos");
                
        $dado = $servico->listarServico($id);
        
        $this->view->servico = $dado;
       
        $retorno = array();
                    
        $retorno['valor'] = utf8_encode($dado->valor);
                
        echo json_encode($retorno); 
    }
    
    public function Excluir_Servico(){
  
        $retorno = array();
        
        if(!empty($_GET['ServicoId'])){
            
            $id = $_GET['ServicoId']; 
                
            $servico = Container::getClass("Servicos");
            
            $result = $servico->delServico($id);
            
            $this->view->retorno = $result;
            
            $this->view->servicos = $servico->getServicos();
            
            $this->render('listar'); 
                    
        }else{
                     
            $retorno["tipo"] = "alert alert-danger alert-dismissible fade show text-center";
            $retorno["mensagem"] = "<strong>Mensagem:</strong> Falha ao obter o Id do Serviço.!";
            
            $this->view->retorno = $result;
 
            $this->view->servicos = $servico->getServicos();
            
            $this->render('listar');
            
        }
        
    }
}