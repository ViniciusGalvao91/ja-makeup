<?php

namespace APP\Controllers;

use SON\Controller\Action;
use \SON\Di\Container;


class Cliente extends Action{
        
    //envia para a tela o conteudo retornado da classe init
    public function cliente(){
        /*
        //instancia a classe "Artigo" do model com a conexao com o db
        $cliente = Container::getClass("Cliente");
        //instancia os metodos da classe "Table" que tem os metodos para o banco
        $clientes = $cliente->fetAll();        
        
        //envia os dados para a view
        $this->view->clientes = $clientes;
        */                
        //action que desejo renderizar
        $this->render('cliente');
    }
    
    public function cadastro(){                      
               
        $this->render('cadastro');
    }
    
    
    //envia para a tela o conteudo retornado da classe init
    public function inserirCliente(){
             
        //if(isset($_POST['nome'])){
            
             //$cliente = "Cliente",$_POST['nome'],$_POST['celular'],$_POST['telefone'],$_POST['email'],$_POST['cpf'],$_POST['obs']);
        
        $cliente = Container::getClass("Cliente");
                                             
        $clientes = $cliente->gravar();        
            
       
        
            //echo "Nenhum dado preenchido"; 
        
       
                
        //$this->view->clientes = $clientes;
        
        //$cliente = Container::getClass("Cliente",$_POST['nome'],$_POST['celular'],$_POST['telefone'],$_POST['email'],$_POST['cpf'],$_POST['obs']);
        
        //$clientes = $cliente->fetAll();        
                
        //$this->view->clientes = $clientes;
               
        //$this->render('cadastro');
    }
    
    public function pesquisa(){
                               
                    
        $cliente = Container::getClass("Cliente");
        
        $clientes = $cliente->fetAll();        
        
    
        $this->view->clientes = $clientes;
        
        //renderizando
        $this->render('pesquisa');
    }
    
    public function pesquisaConsulta(){
                               
        $cliente = Container::getClass("Cliente");
                
        if($clientes = $cliente->find($_POST['id']) == null){
            
           echo ("<script>alert('Cliente não encontrada!');</script>");
            
          
        }else{
        
        $clientes = $cliente->find($_POST['id']);
            //envia os dados para a view
        $this->view->clientes = $clientes;
        
        //renderizando
        $this->render('consulta');
            
        }
    }
    
    
    public function editarCliente(){
       
        //renderizando
        $this->render('editar');
    }
    
    public function excluirCliente(){
       
        //renderizando
        $this->render('excluir');
    }
    
    public function inserir(){
           
       
        $user = array();
     
        foreach($_POST as $dado => $value) { 
            
            $listat = explode('_', $dado);
            $list = explode('_', $value);            
      
            $user[$dado] = $listat[0];
            $user[$dado] = $list[0]; 
        }
               
        $values = implode(",", $user);  
  
                
        foreach($user as $dado =>$valor){
            
            $listat = explode('_', $dado);            
           
            $user[$dado] = $listat[0]; 
            
        }    
        
        $atrib = implode(",", $user);
       
        $str = "(" .$atrib. ") VALUES ('" .$values."')";
            
        //instancia a classe "Artigo" do model com a conexao com o db
        $cliente = Container::getClass("Cliente");
        //instancia os metodos da classe "Table" que tem os metodos para o banco
        $clientes = $cliente->insert($atrib, $values);
            
                
        
        //$clientes = $cliente->registraCliente($atributos,$dados);
        
    }
}