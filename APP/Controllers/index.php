<?php

namespace APP\Controllers;

use SON\Controller\Action;
use \SON\Di\Container;

class Index extends Action{
        
    //envia para a tela o conteudo retornado da classe init
    public function index(){
                        
        //action que desejo renderizar
        $this->render('index');
    }
    
    //envia para a tela o conteudo retornado da classe init
    public function servicos(){
       
        //renderizando
        $this->render('servicos');
    }    
    
    public function orcamentos(){
       
        //renderizando
        $this->render('orcamentos');
    }
    
     public function clientes(){
       
        //renderizando
        $this->render('clientes');
    }
    
        
    public function atendimentos(){
       
        //renderizando
        $this->render('atendimentos');
    }
    
    public function financeiro(){
       
        //renderizando
        $this->render('financeiro');
    }
}

