<?php

namespace APP\Controllers;

use SON\Controller\Action;
use \SON\Di\Container;


class Atendimentos extends Action{
        
    //envia para a tela o conteudo retornado da classe init
    public function Atendimentos(){
        
        $this->Novo_Atendimento();
    }
    
    public function Novo_Atendimento(){
        
        $atendimento = Container::getClass("Atendimentos");
        
        $this->view->atendimento = $atendimento->geraId();
        
        //instancia servicos para mostrar a descricao
        $servico = Container::getClass("Servicos");
         
        $this->view->servicos = $servico->getServicos();
  
        $this->render('Novo_Atendimento');  
        
    }
    
    public function Listar_Atendimento(){
                                         
        $atendimento = Container::getClass("Atendimentos");
        
            
        $this->view->atendimentos = $atendimento->getAtendimentos();
        
        //instancia servicos para mostrar a descricao
        $servico = Container::getClass("Servicos");
         
        $this->view->servicos = $servico->getServicos();
            
        $this->render('Listar_Atendimento');  
    }
    
    public function Mapa(){
                
        //action que desejo renderizar
        $this->render('mapa');
    }
               
    //envia para a tela o conteudo retornado da classe init
    public function salvar(){
             
        $atendimento = Container::getClass("Atendimentos");
        
        $result = $atendimento->salvar($_POST['id'], $_POST['idOrcamento'], $_POST['dataAge'], utf8_decode($_POST['nome']), utf8_decode($_POST['enderecoAtendimento']), utf8_decode($_POST['servico']), $_POST['horaAgendada'], $_POST['valorDeslocamento'], $_POST['faltaReceber']);  
        
        $this->view->atendimento = $atendimento->geraId();
        
        $this->view->retorno = $result;
        
        $this->view->atendimentos = $atendimento->getAtendimentos();
        
        $this->Novo_Atendimento();
                
    }
    
    public function Gerar_Atendimento(){
                    
        $atendimento = Container::getClass("Atendimentos");
        
        $result = $atendimento->salvar($atendimento->geraId(), $_GET['idOrcamento'], $_GET['dataAge'], utf8_decode($_GET['nome']), utf8_decode($_GET['enderecoAtendimento']),  utf8_decode($_GET['servico']), $_GET['horaAgendada'], $_GET['valorDeslocamento'], $_GET['valorTotal'] - $_GET['taxaReserva']);  
        
        $this->view->retorno = $result;
        
        $atendimentos = $atendimento->getAtendimentos();
            
        $this->view->atendimentos = $atendimentos;
        
        $this->render('Listar_Atendimento');
                
    }
            
    public function consulta(){
                               
        $atendimento = Container::getClass("Atendimentos");
        
        $result = $atendimento->getAtendimento($_GET['AtendimentoId']);
        
        //instancia servicos para mostrar a descricao
        $servico = Container::getClass("Servicos");
         
        $this->view->servicos = $servico->getServicos();
            
        //envia os dados para a view
        $this->view->atendimento = $result;
        
        $this->render('Alterar_Atendimento');
                    
    }
    
    public function Excluir_Atendimento(){
  
        $retorno = array();
        
        if(!empty($_GET['AtendimentoId'])){
            
            $id = $_GET['AtendimentoId']; 
                
            $atendimento = Container::getClass("Atendimentos");
            
            $result = $atendimento->deletar($id);
            
            $this->view->retorno = $result;
            
            //instancia servicos para mostrar a descricao
            $servico = Container::getClass("Servicos");
         
            $this->view->servicos = $servico->getServicos();
                        
            $this->view->atendimentos = $atendimento->getAtendimentos();
        
            $this->render('Listar_Atendimento');
                    
        }else{
                     
            $retorno["tipo"] = "alert alert-danger alert-dismissible fade show text-center";
            $retorno["mensagem"] = "<strong>Mensagem:</strong> Falha ao obter o Id do Atendimento.!";
            
            $this->view->retorno = $result;
 
            //instancia servicos para mostrar a descricao
            $servico = Container::getClass("Servicos");
         
            $this->view->servicos = $servico->getServicos();
                        
            $this->view->atendimentos = $atendimento->getAtendimentos();
        
            $this->render('Listar_Atendimento');
            
        }
        
    }
    
    
}