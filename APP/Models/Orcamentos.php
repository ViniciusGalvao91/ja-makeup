<?php

namespace APP\Models;

use SON\Db\Table;

use SON\Db\Config\TLogger;
use SON\Db\Config\TLoggerTXT;

use SON\Db\Ado\TTransaction;

class Orcamentos extends Table{
    
    //aqui eu indico qual a tabela que eu quero consultar. Ex: se eu quiser a tabela usuario é só criar a classe extends table e na variavel table eu colocar "usuario"-
    const TABLENAME = 'orcamentos';   
        
    public function gravar ($id, $dataOrcamento, $nomeCliente, $telefone, $enderecoAtendimento, $formaContato, $tipoEvento, $tipoParticipacao, $diaHora, $servicoSolicitado, $horaAgendada, $deslocamento, $valorTotal, $taxaReserva, $formadePagamento, $statusOrcamento, $obsOrcamento){
        
         try{
        
            TTransaction::open('jamakeup');

            TTransaction::setLogger(new TLoggerTXT('/../../../log_jamakeup/log_Orcamento_'.$this->dataAtual().'.txt'));

            TTransaction::log("**Inserindo Orcamento");
         
            $orcamento = new Orcamentos;
            
            $orcamento->id = $id; 
            $orcamento->nomeCliente = $nomeCliente; 
            $orcamento->telefone = $telefone;
            $orcamento->enderecoAtendimento = $enderecoAtendimento;
            $orcamento->dataOrcamento = $dataOrcamento;
            $orcamento->formaContato = $formaContato;
            $orcamento->tipoEvento = $tipoEvento;
            $orcamento->tipoParticipacao = $tipoParticipacao;
            $orcamento->diaHora = $diaHora;
            $orcamento->servicoSolicitado = $servicoSolicitado;
            $orcamento->horaAgendada = $horaAgendada;
            $orcamento->deslocamento = $deslocamento;
            $orcamento->valorTotal = $valorTotal;
            $orcamento->taxaReserva = $taxaReserva;
            $orcamento->formaPagamento = $formadePagamento;
            $orcamento->statusOrcamento = $statusOrcamento;
            $orcamento->obsOrcamento = $obsOrcamento;
                                               
            $retorno = array();
            
            if($orcamento->store()){
                
               $retorno["tipo"] = "alert alert-success alert-dismissible fade show text-center";
                $retorno["mensagem"] = "<strong>Mensagem:</strong> Orçamento salvo com sucesso!";
                
            }else{
                
                $retorno["tipo"] = "alert alert-danger alert-dismissible fade show text-center";
                $retorno["mensagem"] = "<strong>Mensagem:</strong> Falha ao alterar Orçamento! Nenhuma alteração encontrada.";
                
            }
                                            
            TTransaction::log("**Registros inseridos com sucesso.");
            TTransaction::close();
             
            return $retorno;      
                
        }catch (Exception $e){

            echo '<b>ERRO</b>' . $e->getMessage();
            TTransaction::rollback();
        }
        
    }
    
    /* falta implementar o controlo de ano, pois ao virar o ano ele não esta *    *  alaterando a contagem
     */
    public function geraId(){
    
        try{
        
            TTransaction::open('jamakeup');

            TTransaction::setLogger(new TLoggerTXT('/../../../log_jamakeup/log_Orcamento_'.$this->dataAtual().'.txt'));
            TTransaction::log("**Obtendo Último ID");

            $orcamento = new Orcamentos();
                    
            $id = $orcamento->getLast();
            
            

            TTransaction::close();    

            }catch (Exception $e){

                echo '<b>ERRO</b>' . $e->getMessage();
                TTransaction::rollback();
            }
        
           
        //separa somente o mes
            $mesatual = date("m");
            $mesbase = substr($id,4,2);

            //verifica se existe dado na tabela se nao inicia a contagem
            if(empty($id)){
                
                $id = date("Ym") + $id . 1;
                return $id;
              
            //verifica se o mes ainda é o mesmo dos registros de orcamentos    
            }elseif($mesbase == $mesatual){
                
                $inteiro = substr($id,0,6);
                
                $unidade = substr($id,6,7);
                
                    if($unidade == 9){
                        
                        $unidade+= 1;
                        
                       return $inteiro . $unidade; 
                        
                    }else{
                        
                        return $id + 1;
                
                    }
                        
            //mudando o mes a contagem reinicia dentro do mes novo
            }else{
                
                $id = date("Ym") . 1;

                   return $id;
                    
                }
                
    }
    
    public function data($data){
        
        if($data != "0000-00-00"){
            
            return date("d/m/Y", strtotime($data));
            
        }else{
            
            $data = "";
            return $data;
            
        }
        
    }
    
    public function dateTime($data){
                
        if($data != ""){
            
            return date("d/m/Y H:i", strtotime($data));
            
        }else{
            
            $data = "";
            return $data;
            
        }
        
    }
    
    
    
    public function listarOrcamentos(){
        
        try{
        
            TTransaction::open('jamakeup');

            TTransaction::setLogger(new TLoggerTXT('/../../../log_jamakeup/log_Orcamento_'.$this->dataAtual().'.txt'));

            TTransaction::log("**Obtendo Orçamentos");

            $orcamento = new Orcamentos();
             
            if($orcamento->loadAll()){
                
                $orcamentos = $orcamento->loadAll();
                
                return $orcamentos;  
            }

            TTransaction::close();    

            }catch (Exception $e){

                echo '<b>ERRO</b>' . $e->getMessage();
                TTransaction::rollback();
            }
        
    }
    
    public function listarOrcamento($id){
        
        try{
        
            TTransaction::open('jamakeup');

            TTransaction::setLogger(new TLoggerTXT('/../../../log_jamakeup/log_Orcamento_'.$this->dataAtual().'.txt'));

            TTransaction::log("**Obtendo Orçamento");

            $orcamento = new Orcamentos();
             
            if($orcamento->load($id)){
                
                $result = $orcamento->load($id);
                
                return $result;
                
            }
            
            TTransaction::close();    

            }catch (Exception $e){

                echo '<b>ERRO</b>' . $e->getMessage();
                TTransaction::rollback();
            }
        
    }
    
    public function deletar($id){
        
        try{
        
            TTransaction::open('jamakeup');

            TTransaction::setLogger(new TLoggerTXT('/../../../log_jamakeup/log_Orcamento_'.$this->dataAtual().'.txt'));
            TTransaction::log("**Deletando Orcamento");
                    
            $retorno = array();
            
            $orcamento = new Orcamentos();                   
                        
            if($orcamento->delete($id)){
                
                $retorno["tipo"] = "alert alert-success alert-dismissible fade show text-center";
                $retorno["mensagem"] = "<strong>Mensagem:</strong> Orçamento excluido com sucesso!";
                
            }else{
                
                $retorno["tipo"] = "alert alert-danger alert-dismissible fade show text-center";
                $retorno["mensagem"] = "<strong>Mensagem:</strong> Falha ao excluir Orçamento!";
                
            }
            
            TTransaction::log("**Orcamento excluído com sucesso.");
            TTransaction::close();   
            
            return $retorno;

            }catch (Exception $e){

                echo '<b>ERRO</b>' . $e->getMessage();
                TTransaction::rollback();
            }
    }
    
    public function dataAtual(){
        
            return $data = date("d_m_Y");
              
    }
}