<?php

namespace APP\Models;

use SON\Db\Table;

use SON\Db\Config\TLogger;
use SON\Db\Config\TLoggerTXT;

use SON\Db\Ado\TTransaction;

class Financeiro extends Table{
    
    //aqui eu indico qual a tabela que eu quero consultar. Ex: se eu quiser a tabela usuario é só criar a classe extends table e na variavel table eu colocar "usuario"-
    const TABLENAME = 'financeiro';   
        
    public function gravar($id, $descricao, $dataMovimento, $tipoMovimento, $valorMovimento, $codAtendimento, $meioPagamento, $dataLiberacaoPagSeguro){
        
         try{
        
            TTransaction::open('jamakeup');

            TTransaction::setLogger(new TLoggerTXT('/../../../log_jamakeup/log_Financeiro_'.$this->dataAtual().'.txt'));

            TTransaction::log("**Inserindo Movimento");
         
            $movimento = new Financeiro;
            $movimento->id = $id;
            $movimento->descricao = $descricao;
            $movimento->dataMovimento = $dataMovimento;
            $movimento->tipoMovimento = $tipoMovimento;
            $movimento->valorMovimento = $valorMovimento;
            $movimento->codAtendimento = $codAtendimento;
            $movimento->meioPagamento = $meioPagamento;
            $movimento->dataLiberacaoPagSeguro = $dataLiberacaoPagSeguro;
                                   
            $retorno = array();
            
            if($movimento->store()){
                
                $retorno["tipo"] = "alert alert-success alert-dismissible fade show text-center";
                $retorno["mensagem"] = "<strong>Mensagem:</strong> Lançamento salvo com sucesso!";
                
            }else{
                
                $retorno["tipo"] = "alert alert-danger alert-dismissible fade show text-center";
                $retorno["mensagem"] = "<strong>Mensagem:</strong> Falha ao alterar Lançamento! Nenhuma alteração encontrada.";
                
            }
                                            
            TTransaction::log("**Registros inseridos com sucesso.");
            TTransaction::close();
             
            return $retorno;                 
                
        }catch (Exception $e){

            echo '<b>ERRO</b>' . $e->getMessage();
            TTransaction::rollback();
        }
    }
  
    public function getLancamentos(){
        
        try{
        
            TTransaction::open('jamakeup');

            TTransaction::setLogger(new TLoggerTXT('/../../../log_jamakeup/log_Financeiro_'.$this->dataAtual().'.txt'));

            TTransaction::log("**Obtendo Lançamentos");

            $lancamento = new Financeiro();
                    
            $lancamentos = $lancamento->loadAll();

            return $lancamentos;    

            TTransaction::close();    

            }catch (Exception $e){

                echo '<b>ERRO</b>' . $e->getMessage();
                TTransaction::rollback();
            }
    }
    
    public function getLancamento($id){
        
        try{
        
            TTransaction::open('jamakeup');

            TTransaction::setLogger(new TLoggerTXT('/../../../log_jamakeup/log_Financeiro_'.$this->dataAtual().'.txt'));

            TTransaction::log("**Obtendo Lançamentos");

            $lancamento = new Financeiro();
                    
            $lancamentos = $lancamento->load($id);

            return $lancamentos;    

            TTransaction::close();    

            }catch (Exception $e){

                echo '<b>ERRO</b>' . $e->getMessage();
                TTransaction::rollback();
            }
    }
    
    public function data($data){
        
        if($data != ""){
            
            return date("d/m/Y", strtotime($data));
            
        }else{
            
            $data = "";
            return $data;
            
        }    
    }
    
    public function dataAtual(){
        
            return $data = date("d_m_Y");
              
    }
}