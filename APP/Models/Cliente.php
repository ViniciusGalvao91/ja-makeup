<?php

namespace APP\Models;

use SON\Db\Table;
use SON\Db\Ado;
use SON\Db\Config;

class Cliente extends Table{
    
    //aqui eu indico qual a tabela que eu quero consultar. Ex: se eu quiser a tabela usuario é só criar a classe extends table e na variavel table eu colocar "usuario"-
    const TABLENAME = 'cliente';   
    
    function __autoload($classe){
    
        if(file_exists("../Ado/{$classe}.class.php")){

            include_once "../Ado/{$classe}.class.php";

        }    

        if(file_exists("../Config/{$classe}.class.php")){

            include_once "../Config/{$classe}.class.php";

        }    
    }

    
    public function gravar(){
        
        try{
        
        TTransaction::open('jamakeup');
        
        TTransaction::setLogger(new TLoggerTXT('/tmp/log1.txt'));
        
        TTransaction::log("**Inserindo Cliente");
        
        $cliente = new Cliente;
        $cliente->nome = 'Rosana Duque';
        $cliente->celular = '987071732';
        $cliente->telefone = '26933021';
        $cliente->email = 'rsd@machadomeyer.com.br';
        $cliente->cpf = '13412560731';
        $cliente->obs = 'Teste de insert no banco';
        $cliente->store();
        
        
        //TTransaction::close();
        
        echo "Registros inseridos com sucesso<br>\n";
        
        
        }catch (Exception $e){

            echo '<b>ERRO</b>' . $e->getMessage();
            TTransaction::rollback();
        }
        
    }
    
    
}