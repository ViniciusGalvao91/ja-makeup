<?php

namespace APP\Models;

use SON\Db\Table;

use SON\Db\Config\TLogger;
use SON\Db\Config\TLoggerTXT;

use SON\Db\Ado\TTransaction;

class Servicos extends Table{
    
    //aqui eu indico qual a tabela que eu quero consultar. Ex: se eu quiser a tabela usuario é só criar a classe extends table e na variavel table eu colocar "usuario"
    const TABLENAME = 'servicos';    
    

    public function salvar($id, $nomeservico, $descricaoServico, $valor, $status){
        
         try{
        
            TTransaction::open('jamakeup');

            TTransaction::setLogger(new TLoggerTXT('/../../../log_jamakeup/log_Servico_'.$this->dataAtual().'.txt'));

            TTransaction::log("**Inserindo Serviço");
         
            $servico = new Servicos;
            $servico->id = $id;
            $servico->nomeServico = $nomeservico;
            $servico->descricaoServico = $descricaoServico;
            $servico->valor = $valor;
            $servico->status = $status; 
                                                
            $retorno = array();
            
            if($servico->store()){
                
                $retorno["tipo"] = "alert alert-success alert-dismissible fade show text-center";
                $retorno["mensagem"] = "<strong>Mensagem:</strong> Serviço salvo com sucesso!";
                
            }else{
                
                $retorno["tipo"] = "alert alert-danger alert-dismissible fade show text-center";
                $retorno["mensagem"] = "<strong>Mensagem:</strong> Falha ao alterar Serviço! Nenhuma alteração encontrada.";
                
            }
                                            
            TTransaction::log("**Registros inseridos com sucesso.");
            TTransaction::close();
             
            return $retorno;
                
        }catch (Exception $e){

            echo '<b>ERRO</b>' . $e->getMessage();
            TTransaction::rollback();
        }
    }
       
    public function getServicos(){
        
        try{
        
            TTransaction::open('jamakeup');

            TTransaction::setLogger(new TLoggerTXT('/../../../log_jamakeup/log_Servico_'.$this->dataAtual().'.txt'));

            TTransaction::log("**Obtendo Serviços");

            $servico = new Servicos();
                    
            $servicos = $servico->loadAll();

            return $servicos;    

            TTransaction::close();    

            }catch (Exception $e){

                echo '<b>ERRO</b>' . $e->getMessage();
                TTransaction::rollback();
            }
    }
    
     public function listarServico($id){
        
        try{
        
            TTransaction::open('jamakeup');

            TTransaction::setLogger(new TLoggerTXT('/../../../log_jamakeup/log_Servico_'.$this->dataAtual().'.txt'));

            TTransaction::log("**Obtendo Serviço");

            $servico = new Servicos();
             
            if($servico->load($id)){
                
                $result = $servico->load($id);
                
                return $result;
                
            }
            
            TTransaction::close();    

            }catch (Exception $e){

                echo '<b>ERRO</b>' . $e->getMessage();
                TTransaction::rollback();
            }
        
    }
      
    public function delServico($id){
        
        try{
        
            TTransaction::open('jamakeup');

            TTransaction::setLogger(new TLoggerTXT('/../../../log_jamakeup/log_Servico_'.$this->dataAtual().'.txt'));

            TTransaction::log("**Deletando Serviço");

            $servico = new Servicos();
             
            if($servico->delete($id)){
                
                $retorno["tipo"] = "alert alert-success alert-dismissible fade show text-center";
                $retorno["mensagem"] = "<strong>Mensagem:</strong> Serviço excluido com sucesso!";
                
            }else{
                
                $retorno["tipo"] = "alert alert-danger alert-dismissible fade show text-center";
                $retorno["mensagem"] = "<strong>Mensagem:</strong> Falha ao excluir Serviço!";
                
            }
                                            
            TTransaction::log("**Serviço excluído com sucesso.");
            TTransaction::close();
             
            return $retorno;    

            }catch (Exception $e){

                echo '<b>ERRO</b>' . $e->getMessage();
                TTransaction::rollback();
            }
        
    }
    
    public function dataAtual(){
        
            return $data = date("d_m_Y");
              
    }
}