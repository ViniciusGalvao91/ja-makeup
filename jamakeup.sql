-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 06-Maio-2019 às 03:35
-- Versão do servidor: 10.1.32-MariaDB
-- PHP Version: 7.0.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jamakeup`
--
CREATE DATABASE IF NOT EXISTS `jamakeup` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `jamakeup`;

-- --------------------------------------------------------

--
-- Estrutura da tabela `atendimentos`
--

DROP TABLE IF EXISTS `atendimentos`;
CREATE TABLE IF NOT EXISTS `atendimentos` (
  `id` int(11) NOT NULL,
  `idOrcamento` int(11) NOT NULL,
  `dataAge` date NOT NULL,
  `nome` varchar(60) NOT NULL,
  `enderecoAtendimento` varchar(100) NOT NULL,
  `servico` varchar(40) NOT NULL,
  `horaAgendada` char(5) NOT NULL,
  `valorDeslocamento` float NOT NULL,
  `faltaReceber` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `atendimentos`
--

INSERT INTO `atendimentos` (`id`, `idOrcamento`, `dataAge`, `nome`, `enderecoAtendimento`, `servico`, `horaAgendada`, `valorDeslocamento`, `faltaReceber`) VALUES
(2, 0, '2019-01-18', 'Maria Clara (Makes da Maria)', 'Rua Élvio de Moura,217 - Ricardo de Albuquerque', '4', '18:00', 0, 100),
(3, 0, '2019-01-20', 'Bianca Andrade Gomes', '', '4', '11:30', 0, 100),
(5, 0, '2019-01-20', 'Yasmin Novaes', 'Rua Elvio de Moura, 217 - Ricardo de Albuquerque', '19', '18:00', 20, 270),
(6, 0, '2019-02-22', 'Aline Viana', 'Rua Elvio de Moura, 217 - Ricardo de Albuquerque', '2', '10:00', 0, 25),
(7, 0, '2019-02-24', 'Carolina Galvão', 'Estr. Antônio José Bitencourt, 70 - Centro, Nilópolis - RJ', '4', '09:00', 20, 120),
(8, 0, '2019-03-07', 'Juliana Silva ', 'Rua Elvio de Moura, 217 - Ricardo de Albuquerque', '2', '11:00', 0, 25),
(9, 0, '2019-03-09', 'Thais Abreu ', 'Estr. Nilo Peçanha, 853 - Olinda, Nilópolis - RJ', '18', '14:00', 20, 220),
(10, 0, '2019-03-13', 'Carolina Candido', 'Rua Elvio de Moura, 217 - Ricardo de Albuquerque', '2', '18:00', 0, 25),
(11, 0, '2019-03-16', 'Vanessa Costa ', 'R. Eliseu de Alvarenga, 828 - Olinda, Nilópolis', '20', '21:00', 25, 300),
(12, 0, '2019-03-23', 'Rose (Wando)', 'R. Antônio João Mendonça, 277 - Centro, Nilópolis', '4', '18:30', 0, 80),
(13, 0, '2019-03-30', 'Laísa Freire', 'R. Namur, 602 - 303 - Vila Valqueire ', '4', '13:00', 70, 170),
(14, 0, '2019-03-30', 'Ana Luisa', 'Rua Monsenhor Felix  1059 bloco 80/ 4 apartamento 611. Irajá', '8', '16:30', 35, 135),
(15, 0, '2019-03-30', 'Vaneska', 'Rua Monsenhor Felix  1059 bloco 80/ 4 apartamento 611. Irajá', '4', '18:00', 35, 135),
(16, 0, '2019-03-30', 'Jéssica Galvão', 'R. Marquês Canário- Nossa Sra. de Fatima, Nilópolis ', '18', '19:00', 0, 200),
(17, 0, '2019-04-06', 'Larissa Berkowitz', 'Tv. Hilton, 73 - Santos Dumont, Nilópolis', '2', '10:00', 0, 25),
(18, 0, '2019-04-13', 'Nathalia Scheneider', 'Rua Francisca Sales, 374, casa 216. Taquara', '4', '13:00', 80, 180),
(19, 0, '2019-04-21', 'Maria Clara', 'Rua Machado de Assis, 364 - Imperador, Nova Iguaçu - RJ', '20', '16:00', 0, 270),
(20, 0, '2019-04-21', 'Claudia Mara', 'Rua Machado de Assis, 364 - Imperador, Nova Iguaçu - RJ', '11', '16:00', 20, 200),
(21, 0, '2019-04-21', 'Claudia Mara (Irmã)', 'Rua Machado de Assis, 364 - Imperador, Nova Iguaçu - RJ', '4', '16:00', 20, 120),
(22, 2019053, '2019-03-26', 'Ingrid', 'R. Jose Manieiro,14 - Nossa Sra. de Fatima, Nilópolis', 'Maquiagem Completa', '06:00', 20, 70),
(23, 0, '2019-04-27', 'Aline Viana', 'Rua Elvio de Moura, 217 - Ricardo de Albuquerque', '2', '10:00', 0, 25),
(24, 2019041, '2019-04-26', 'Anna Torres ', 'R. Quintino Bocaiúva, 313 - Nova Cidade, Nilópolis - RJ', 'Design de Sobrancelhas com Henna ', '12:00', 50, 200),
(25, 2019051, '2019-04-12', 'Taynara Lima', 'R. Doná Olga, 70 - Silvânia, Nova Iguaçu - RJ, 26015-005, Brasil', 'Pacote Simples op??o 0 - Curtos', '08:00', 0, 100);

-- --------------------------------------------------------

--
-- Estrutura da tabela `financeiro`
--

DROP TABLE IF EXISTS `financeiro`;
CREATE TABLE IF NOT EXISTS `financeiro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(100) NOT NULL,
  `dataMovimento` date NOT NULL,
  `tipoMovimento` char(7) NOT NULL,
  `valorMovimento` float NOT NULL,
  `codAtendimento` int(11) NOT NULL,
  `meioPagamento` char(12) NOT NULL,
  `dataLiberacaoPagSeguro` char(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `orcamentos`
--

DROP TABLE IF EXISTS `orcamentos`;
CREATE TABLE IF NOT EXISTS `orcamentos` (
  `id` int(11) NOT NULL,
  `nomeCliente` varchar(60) NOT NULL,
  `telefone` char(13) NOT NULL,
  `enderecoAtendimento` varchar(100) NOT NULL,
  `dataOrcamento` date NOT NULL,
  `formaContato` char(9) NOT NULL,
  `tipoEvento` varchar(20) NOT NULL,
  `tipoParticipacao` varchar(25) NOT NULL,
  `diaHora` char(16) NOT NULL,
  `servicoSolicitado` int(11) NOT NULL,
  `horaAgendada` char(5) NOT NULL,
  `deslocamento` float NOT NULL,
  `valorTotal` float NOT NULL,
  `taxaReserva` float NOT NULL,
  `formaPagamento` char(13) NOT NULL,
  `statusOrcamento` varchar(35) NOT NULL,
  `obsOrcamento` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `orcamentos`
--

INSERT INTO `orcamentos` (`id`, `nomeCliente`, `telefone`, `enderecoAtendimento`, `dataOrcamento`, `formaContato`, `tipoEvento`, `tipoParticipacao`, `diaHora`, `servicoSolicitado`, `horaAgendada`, `deslocamento`, `valorTotal`, `taxaReserva`, `formaPagamento`, `statusOrcamento`, `obsOrcamento`) VALUES
(2018121, 'Paula Castro', '21 97507-', 'Estr. do Rio Grande, 2476 - Taquara, Rio de Janeiro - RJ, 22720-010, Brasil', '2018-12-03', 'WhatsApp', 'Casamento', 'Noiva', '2019-09-14T17:00', 27, '', 0, 1640, 200, 'Boleto', 'Aprovado', 'Valor total :: 1540,00. // - Noiva = 200,00 reser ok / Madrinha 1 = 50,00 reser ok / Madrinha 2 = -50,00 reser ok\r\nPrévia: 10/08 às 17H. '),
(2019021, 'Jessica Galvão', '21 99655-', ' R. Marquês Canário- Nossa Sra. de Fatima, Nilópolis ', '2019-02-18', 'WhatsApp', 'Casamento', 'Madrinha', '2019-05-25T18:00', 19, '15:00', 0, 250, 0, '', 'Aprovado', ''),
(2019031, 'Carolina Candido', '21 98824-', 'R. Estér, 66 - Olinda, Nilópolis - RJ', '2019-03-13', 'WhatsApp', 'Formatura', 'Formanda', '2019-06-08T19:00', 18, '15:00', 50, 250, 50, 'Transferência', 'Aprovado', 'Ponto de referência: Prox. ao Brunos Festas e Igreja São Sebastião. // Não precisar dar nada no dia pois já pagou o valor integral.'),
(2019032, 'Marilia Nogueira', ' 21 96859', 'Sítio Cabungui. Estrada do Cabungui, 780, Vargem grande', '2019-03-19', 'WhatsApp', 'Casamento', 'Noiva', '2019-09-07T15:00', 0, '11:00', 0, 0, 0, 'Transferência', 'Aprovado', 'Meu endereço é Avenida Dom Helder Câmara 4880, BL. 1, 203, Cachambi.'),
(2019033, 'Ingrid', '97 8405-6', 'R. Jose Manieiro,14 - Nossa Sra. de Fatima, Nilópolis', '2019-03-26', 'WhatsApp', 'Ensaio', 'Modelo', '2019-04-27T07:00', 4, '06:00', 20, 120, 50, 'Boleto', 'Aprovado', 'Ensaio gestante'),
(2019034, 'Ingrid', '97 8405-6', 'R. Jose Manieiro,14 - Nossa Sra. de Fatima, Nilópolis', '2019-03-26', 'WhatsApp', 'Ensaio', 'Modelo', '2019-05-03T14:00', 4, '12:00', 20, 120, 50, 'Boleto', 'Aprovado', 'Ensaio gestante'),
(2019041, 'Carolina Galvão', '21 98360-', 'Estr. Antônio José Bitencourt, 70 - Centro, Nilópolis - RJ', '2019-04-01', 'WhatsApp', 'Cha de Bebe', 'Mãe da Aniversariante', '2019-06-15T17:00', 4, '15:00', 20, 120, 0, '', 'Aprovado', 'Não pagou taxa de reserva. Vai dar tudo no dia do evento.'),
(2019042, 'Victória Cardoso', '21 98091-', 'Tv. Etelvina Tomaz Souza, 77 - Centro, Mesquita - RJ', '2019-04-08', 'WhatsApp', 'Casamento', 'Madrinha', '2019-06-01T19:30', 20, '15:30', 0, 270, 50, 'Boleto', 'Aprovado', 'Ponto de referência: travessa Egídio 77. / - Próximo ao cemitério de mesquita. / - Em frente a garagem do Mirante Vila Rica\r\nFalta receber 220,00 no dia.'),
(2019043, 'Taynara Lima', '988795057', 'R. Doná Olga, 70 - Silvânia, Nova Iguaçu - RJ, 26015-005, Brasil', '2019-04-12', 'WhatsApp', 'Festa', 'Mãe da Aniversariante', '2019-05-11T12:00', 35, '08:00', 0, 150, 50, 'Boleto', 'Aprovado', 'Ponto de referência: Próximo a dutra. Depois da Rio Sampa.\r\nTem que estar pronta ás 10H00.'),
(2019044, 'Tatiana Diniz', '21 99305-', 'Rua Elvio de Moura, 217 - Ricardo de Albuquerque', '2019-04-26', 'WhatsApp', 'Casamento', 'Madrinha', '2019-12-14T20:00', 18, '00:00', 50, 250, 50, 'Transferência', 'Enviado', ''),
(2019045, 'Anna Torres ', '975437081', 'R. Quintino Bocaiúva, 313 - Nova Cidade, Nilópolis - RJ', '2019-04-26', 'WhatsApp', 'Casamento', 'Convidada', '2019-04-27T20:00', 2, '12:00', 50, 250, 50, 'Dinheiro', 'Aprovado', ''),
(2019051, 'Amanda Oliveira', '982749961', 'Rua Elvio de Moura, 217 - Ricardo de Albuquerque', '2019-05-03', 'WhatsApp', '', '', '', 10, '', 0, 180, 0, '', '', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `servicos`
--

DROP TABLE IF EXISTS `servicos`;
CREATE TABLE IF NOT EXISTS `servicos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomeServico` varchar(60) NOT NULL,
  `descricaoServico` varchar(100) NOT NULL,
  `valor` float NOT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `servicos`
--

INSERT INTO `servicos` (`id`, `nomeServico`, `descricaoServico`, `valor`, `status`) VALUES
(1, 'Design de sobrancelhas ', 'Design de sobrancelhas ', 15, 1),
(2, 'Design de Sobrancelhas com Henna ', 'Design de sobrancelhas com aplicação de henna', 25, 1),
(3, 'Aula de Automaquiagem', 'Design de Sobrancelhas ', 120, 1),
(4, 'Maquiagem Completa', 'Maquiagem social completa para eventos ', 100, 1),
(5, 'Babyliss ou Prancha - Curtos ', 'Escova e babyliss/prancha em cabelos acima dos ombros', 80, 1),
(6, 'Babyliss ou Prancha - Médios', 'Escova e babyliss/prancha em cabelos do ombro até meio das costas', 120, 1),
(7, 'Babyliss ou Prancha - Longos', 'Escova e babyliss/prancha em cabelos do meio das costas para baixo', 150, 1),
(8, 'Penteado Completo - Curtos', 'Preparação e penteado em cabelos acima dos ombros', 100, 1),
(9, 'Penteado Completo - Médios', 'Preparação e penteado em cabelos do ombro até o meio das costas', 150, 1),
(10, 'Penteado Completo - Longos', 'Preparação e penteado em cabelos do meio das costas para baixo', 180, 1),
(11, 'Pacote Simples opção 1 -  Curtos', 'Maquiagem, escova e babyliss/prancha em cab. acima dos ombros', 180, 1),
(16, 'Pacote Simples opção 1 -  Médios', 'Maquiagem, escova e babyliss/prancha em cab. do ombro até meio das costas', 220, 1),
(17, 'Pacote Simples opção 1 -  Longos', 'Maquiagem, escova e babyliss/prancha em cab. do meio das costas para baixo', 250, 1),
(18, 'Pacote Simples opção 2 -  Curtos', 'Maquiagem, preparação e penteado em cab. acima dos ombros', 200, 1),
(19, 'Pacote Simples opção 2 -  Médios', 'Maquiagem, preparação e penteado em cab. do ombro até o meio das costas', 250, 1),
(20, 'Pacote Simples opção 2 -  Longos', 'Maquiagem, preparação e penteado em cab. do meio das costas para baixo', 270, 1),
(21, 'Pacote Debuntante ', 'Ensaio simples, prévia e dia do evento', 1200, 1),
(23, 'Pacote Debuntante + 1 acomp.', 'Ensaio simples, prévia e dia do evento (debut. e acomp.) ', 1390, 1),
(24, 'Pacote Debuntante + 2 acomp.', 'Ensaio simples, prévia e dia do evento (debut. e acomp.) ', 1540, 1),
(25, 'Pacote Noiva', 'Ensaio simples, prévia e dia do evento ', 1300, 1),
(26, 'Pacote Noiva + 1 acomp.', 'Ensaio simples, prévia e dia do evento (noiva. e acomp.) ', 1490, 1),
(27, 'Pacote Noiva + 2 acomp.', 'Ensaio simples, prévia e dia do evento (noiva. e acomp.) ', 1640, 1),
(28, 'Maquiagem Kids', 'Maquiagem voltada para o publico infantil', 30, 1),
(29, 'Pacote Simples opção 1 - Kids Curtos', 'Maquiagem, escova e babyliss/prancha em cabelos acima dos ombros', 110, 1),
(30, 'Pacote Simples opção 1 - Kids Médios', 'Maquiagem, escova e babyliss/prancha em cabelos do ombro até meio das costas', 150, 1),
(31, 'Pacote Simples opção 1 - Kids Longos', 'Maquiagem, escova e babyliss/prancha em cab. do meio das costas para baixo', 180, 1),
(32, 'Pacote Simples opção 2 - Kids Curtos', 'Maquiagem, preparação e penteado em cab. acima dos ombros', 130, 1),
(33, 'Pacote Simples opção 2 - Kids Médios', 'Maquiagem, preparação e penteado em cab. do ombro até o meio das costas', 180, 1),
(34, 'Pacote Simples opção 2 - Kids Longos', 'Maquiagem, preparação e penteado em cab. do meio das costas para baixo', 205, 1),
(35, 'Pacote Simples opção 0 - Curtos', 'Maquiagem e escova em cabelos acima dos ombros', 150, 1),
(36, 'Pacote Simples opção 0 - Médios', 'Maquiagem e escova em cabelos do ombro até o meio das costas', 220, 1),
(37, 'Pacote Simples opção 0 -  Longos', 'Maquiagem e escova em cab. do meio das costas para baixo', 250, 1);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
